import { createStore } from 'redux';
import rootReducer from './reducers';

// Create the Redux store
// rootReducer is the combined reducer that manages the entire state tree
// initialData could be used to initialize the state if needed (not used in this code)
const store = createStore(rootReducer);

// Export the store to make it available to the entire application
export default store;
