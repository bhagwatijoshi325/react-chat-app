import dummyData from '../../data/dummyData.json';

// Initial state for the conversations reducer
const initialState = {
  conversations: dummyData.conversations,
  selectedConversation: null,
  contacts: dummyData.contacts
};

// Action types
export const START_CONVERSATION = 'START_CONVERSATION';
export const SEND_MESSAGE = 'SEND_MESSAGE';

// Action creator for starting a conversation
export const startConversation = (contactId) => ({
  type: START_CONVERSATION,
  payload: contactId,
});

// Action creator for sending a message
export const sendMessage = (conversationId, message) => ({
  type: SEND_MESSAGE,
  payload: { conversationId, message },
});

// Conversations reducer to manage the conversations slice of the state
const conversationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_CONVERSATION:
      // Find existing conversation or create a new one
      const existingConversation = state.conversations.find(
        (conversation) => conversation.contactId === action.payload
      );

      let updatedConversations = state.conversations;
      let updatedSelectedConversation;

      if (existingConversation) {
        updatedSelectedConversation = existingConversation;
      } else {
        const newConversation = {
          id: state.conversations.length + 1,
          contactId: action.payload,
          messages: [],
        };
        updatedConversations = [...state.conversations, newConversation];
        updatedSelectedConversation = newConversation;
      }

      return {
        ...state,
        conversations: updatedConversations,
        selectedConversation: updatedSelectedConversation,
      };

    case SEND_MESSAGE:
      // Update conversations with the new message
      const updatedConversationsWithMessage = state.conversations.map((conversation) => {
        if (conversation.id === action.payload.conversationId) {
          return {
            ...conversation,
            messages: [...conversation.messages, action.payload.message],
          };
        }
        return conversation;
      });

      // Find the selected conversation
      let selectedConversation = updatedConversationsWithMessage.find((obj) => obj.id === action.payload.conversationId);
      return {
        ...state,
        conversations: updatedConversationsWithMessage,
        selectedConversation: { ...selectedConversation }
      };

    default:
      return state;
  }
};

// Export the conversations reducer
export default conversationsReducer;
