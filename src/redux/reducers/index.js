import { combineReducers } from 'redux';
import conversationsReducer from './conversations';

// Combine all the reducers into a single root reducer
// The key 'conversations' defines the slice of the state managed by the conversationsReducer
const rootReducer = combineReducers({
  conversations: conversationsReducer,
});

// Export the root reducer to be used when creating the Redux store
export default rootReducer;
