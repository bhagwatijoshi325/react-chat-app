import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Sidebar from './components/Sidebar/Sidebar';
import ChatView from './components/ChatView/ChatView';
import './App.css';
import { Provider } from 'react-redux';
import store from './redux/store';

// Main App component
const App = () => {
  return (
    // Redux Provider to make the store available to all container components
    <Provider store={store}>
      {/* React Router to handle navigation */}
      <h1 style={{textAlign: 'center'}}> React Chat App </h1>
      <Router>
        <div className="app">
          {/* Sidebar component to display the list of conversations */}
          <Sidebar />
          {/* Routes to define the application's navigation paths */}
          <Routes>
            {/* Main chat view route */}
            <Route path="/" element={<ChatView />} />
            {/* Additional routes can be added here */}
          </Routes>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
