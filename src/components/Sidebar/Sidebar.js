import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startConversation } from '../../redux/reducers/conversations';
import dummyData from '../../data/dummyData.json'; // Adjust the path to your dummy data file

const Sidebar = () => {
  const dispatch = useDispatch();
  const conversations = useSelector((state) => state.conversations.conversations);
  const contacts = useSelector((state) => state.conversations.contacts);


  const [showPopup, setShowPopup] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');

  const handleSelectContact = (contact) => {
    dispatch(startConversation(contact.id));
    setShowPopup(false);
  };

  const filteredConversations = conversations.filter((conversation) =>
    // conversation?.id?.toLowerCase().includes(searchTerm.toLowerCase()) || false
    {
      let contactName = contacts.find((obj)=>{
        return obj.id === conversation.id;
      }).name;
      return contactName.toLowerCase().includes(searchTerm.toLowerCase());
    }
  );

  console.log('filteredConversations : ', filteredConversations );
  console.log('converstaions : ', conversations );

  return (
    <div className="sidebar">
      <div className="conversations-list">
        <input
          type="text"
          placeholder="Search Conversations"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        {filteredConversations.map((conversation) => (
          <div key={conversation.id} className="conversation-item">
            {/* <h4>{conversation.contactName}</h4> */}
            <h5>{contacts.find((obj) => {
              return obj.id === conversation.id;
            }).name}</h5>
            <p>{conversation.messages[conversation.messages.length - 1]?.text || 'No messages'}</p>
          </div>
        ))}
      </div>
      <button onClick={() => setShowPopup(true)}>Create Conversation</button>

      {showPopup && (
        <div className="popup">
          <h3>Select a Contact</h3>
          {dummyData.contacts.map((contact) => (
            <div key={contact.id} className="contact-item" onClick={() => handleSelectContact(contact)}>
              <h4>{contact.name}</h4>
            </div>
          ))}
          <button onClick={() => setShowPopup(false)}>Close</button>
        </div>
      )}
    </div>
  );
};

export default Sidebar;
