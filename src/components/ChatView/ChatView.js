import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { sendMessage } from '../../redux/reducers/conversations';

// ChatView component to display the active conversation and send messages
const ChatView = () => {
  const dispatch = useDispatch();

  // Retrieve the selected conversation from the Redux store
  let selectedConversation = useSelector((state) => state.conversations);
  selectedConversation = selectedConversation.selectedConversation;

  // Local state to manage the message input field
  const [messageInput, setMessageInput] = useState('');

  // Function to handle sending a message
  const handleSendMessage = (e) => {
    e.preventDefault();
    if (selectedConversation && messageInput.trim() !== '') {
      // Create the message object
      const message = {
        sender: 'user',
        text: messageInput.trim(),
      };

      // Dispatch the sendMessage action to the Redux store
      dispatch(sendMessage(selectedConversation.id, message));

      // Clear the input field
      setMessageInput('');
    }
  };

  return (
    <div className="chat-view">
      {selectedConversation ? (
        <>
          {/* Chat header displaying the contact name */}
          <div className="chat-header">
            <h3>{selectedConversation.contactName}</h3>
          </div>
          {/* List of messages in the conversation */}
          <div className="chat-messages">
            {selectedConversation.messages.map((message, index) => (
              <div key={index} className={`message-${message.sender === 'user' ? 'you' : 'other'}`}>
                <p>{message.text}</p>
              </div>
            ))}
          </div>
          {/* Input form to type and send messages */}
          <form className="chat-input" onSubmit={handleSendMessage}>
            <input
              type="text"
              value={messageInput}
              onChange={(e) => setMessageInput(e.target.value)}
              placeholder="Type a message..."
            />
            <button type="submit">Send</button>
          </form>
        </>
      ) : (
        // Placeholder text when no conversation is selected
        <div className="no-conversation">
          <p>Select a conversation to start chatting.</p>
        </div>
      )}
    </div>
  );
};

export default ChatView;
