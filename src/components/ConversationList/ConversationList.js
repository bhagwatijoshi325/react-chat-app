import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startConversation } from '../../redux/reducers/conversations';

const ConversationList = () => {
  const dispatch = useDispatch();
  const conversations = useSelector((state) => state.conversations.conversations);

  const handleSelectConversation = (conversation) => {
    dispatch(startConversation(conversation.contactId));
  };

  return (
    <div className="conversations-list">
      {conversations.map((conversation) => (
        <div
          key={conversation.id}
          className="conversation-item"
          onClick={() => handleSelectConversation(conversation)}
        >
          <h4>{conversation.contactName}</h4>
          <p>{conversation.messages[conversation.messages.length - 1]?.text || 'No messages'}</p>
        </div>
      ))}
    </div>
  );
};

export default ConversationList;
