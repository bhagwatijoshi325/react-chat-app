import React from 'react';
import { useDispatch } from 'react-redux';
import { startConversation } from '../../redux/reducers/conversations';
import dummyData from '../../data/dummyData.json'; // Importing dummy data for contacts

// ContactPopup component to display a list of contacts and allow the user to start a conversation
const ContactPopup = ({ setShowPopup }) => {
  const dispatch = useDispatch();

  // Function to handle the selection of a contact and start a conversation
  const handleSelectContact = (contact) => {
    // Dispatch the startConversation action with the selected contact's ID
    dispatch(startConversation(contact.id));

    // Close the popup
    setShowPopup(false);
  };

  return (
    <div className="contact-popup">
      <h3>Select a Contact</h3>
      <div>
        {/* Iterate through the contacts and display each one */}
        {dummyData.contacts.map((contact) => (
          <div
            key={contact.id}
            onClick={() => handleSelectContact(contact)}
            style={{ cursor: 'pointer', padding: '5px' }}
          >
            {contact.name}
          </div>
        ))}
      </div>
      {/* Button to close the popup */}
      <button onClick={() => setShowPopup(false)}>Close</button>
    </div>
  );
};

// Export the ContactPopup component
export default ContactPopup;
