# Chat Application Frontend

A frontend application for a chat system, built using React. This project provides a user-friendly interface to interact with conversations, send messages, and manage contacts.

## Key Functionalities

### Left Sidebar:
- **Search Conversations**: Conversations are searchable by contact name.
- **Conversations List**: Show all the conversations created with the contact name and some text of the last message in the chat.
- **Create Conversation Button**: Opens a popup to load all the contacts and start a new conversation or open an existing one.

### Right Side View:
- **Selected Conversation**: Show the current selected conversation messages.
- **Send Message**: Allows sending a message in the conversation.
- **Alerts/Notifications**: Handle errors and success alerts, showing appropriate notifications.

### Bonus Feature:
- **Redux Data Persistence**: Make the Redux data persistent such that after refresh, the messages and conversations are intact.

## Technologies Employed

- React (using Hooks ONLY)
- React-Router
- Redux

## Requirements

For development, you will need Node.js (16+), a node global package (Npm).

### Node Installation

#### Windows
[Official Node.js website](https://nodejs.org/)

#### Ubuntu
```bash
$ sudo apt install nodejs
$ sudo apt install npm
```

#### Other Operating Systems
[Official Node.js website](https://nodejs.org/)
[Official NPM website](https://www.npmjs.com/)

### Verify Installation
```bash
$ node --version 
V16.xx.xx
$ npm --version 
x.xx.xx
```

### Update NPM
```bash
$ npm install npm -g
```

## Install Project

```bash
$ git clone https://gitlab.com/bhagwatijoshi325/react-chat-app.git
$ cd chat-application-frontend
$ npm install
```

## Running the Project on Development

Set up \`.env\` file in the root, format is given in the \`.env.example\`

```bash
$ npm start
```


### Directory Structure
```
[CHATAPP]
│   .gitignore
│   package-lock.json
│   package.json
│   README.md
│   
├───public
│       favicon.ico
│       index.html
│       logo192.png
│       logo512.png
│       manifest.json
│       robots.txt
│
└───src
    │   App.css
    │   App.js
    │   App.test.js
    │   index.css
    │   index.js
    │   logo.svg
    │   reportWebVitals.js
    │   setupTests.js
    │
    ├───assets
    ├───components
    │   │   ContactPopup.js
    │   │
    │   ├───ChatView
    │   │       ChatView.js
    │   │
    │   ├───ConversationList
    │   │       ConversationList.js
    │   │
    │   └───Sidebar
    │           Sidebar.js
    │
    ├───data
    │       dummyData.json
    │
    ├───redux
    │   │   store.js
    │   │
    │   └───reducers
    │           conversations.js
    │           index.js
    │
    └───views
```

## Support

Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.